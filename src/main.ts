import * as Phaser from 'phaser';
import { EndingScene } from './scenes/ending';
import { GameScene } from './scenes/game/game';
import { GameoverScene } from './scenes/gameover';

const gameConfig: Phaser.Types.Core.GameConfig = {
  type: Phaser.WEBGL,
  width: 64,
  height: 64,
  parent: 'game',
  backgroundColor: '#1b2632',
  zoom: 10,
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
    },
  },
  render: {
    pixelArt: true,
  },
  scene: [GameScene, EndingScene, GameoverScene],
};

export const game = new Phaser.Game(gameConfig);
