import { newGameWithEnemy, newCharacter, newGameWithPlayer } from './gamestate.test.helper';
import { goToNextScene, goToGameOverScene } from './scene';
import { MockScene } from './scene.test.helper';

describe('goToNextScene', () => {
  it('do not start next scene if enemy hp still greater than 0', () => {
    const scene = new MockScene();

    goToNextScene(newGameWithEnemy(newCharacter(0, 0, 10)), scene, 0);

    expect(scene.startHasBeenCalled).toBeFalsy();
  });

  it('Go to next level if enemy hp equals to 0', () => {
    const scene = new MockScene();

    goToNextScene(newGameWithEnemy(newCharacter(0, 0, 0)), scene, 0);

    expect(scene.startHasBeenCalled).toBeTruthy();
    expect(scene.nextScene).toEqual('Game');
    expect(scene.nextSceneData).toEqual({ level: 1 });
  });

  it('Go to ending scene if reached the last levels', () => {
    const scene = new MockScene();

    goToNextScene(newGameWithEnemy(newCharacter(0, 0, 0)), scene, 2);

    expect(scene.startHasBeenCalled).toBeTruthy();
    expect(scene.nextScene).toEqual('Ending');
  });
});

describe('goToGameoverScene', () => {
  it('do nothing when player HP is greater than 0', () => {
    const game = newGameWithPlayer(newCharacter(0, 0, 10));
    const scene = new MockScene();

    goToGameOverScene(game, scene);

    expect(scene.startHasBeenCalled).toBeFalsy();
  });

  it('go to gameover scene when player hp is 0', () => {
    const game = newGameWithPlayer(newCharacter(0, 0, 0));
    const scene = new MockScene();

    goToGameOverScene(game, scene);

    expect(scene.nextScene).toEqual('Gameover');
  });
});
