import { Bullet } from './bullet';
import { Character, Weapon } from './character';
import { SpriteBody, Sprite } from './game';
import { GameState } from './gamestate';

export function newCharacter(x: number, dir: number, hp: number): Character {
  return {
    x: x,
    y: 60,
    dir: dir,
    hp: hp,
    weapon: {
      xOffset: 0,
      yOffset: 0,
      fireRateCountdown: 0,
      fireRate: 10,
      ready: true,
      dir: -1,
    },
    speed: 1,
  };
}

export function newCharacterAt(x: number, y: number, dir: number, hp: number): Character {
  return {
    x: x,
    y: y,
    dir: dir,
    hp: hp,
    weapon: {
      xOffset: 0,
      yOffset: 0,
      fireRateCountdown: 0,
      fireRate: 10,
      ready: true,
      dir: -1,
    },
    speed: 1,
  };
}

export function newCharacterWithWeapon(x: number, dir: number, hp: number, weapon: Weapon): Character {
  return {
    x: x,
    y: 60,
    dir: dir,
    hp: hp,
    weapon: weapon,
    speed: 1,
  };
}

export function newCharacterWithWeaponAt(x: number, y: number, dir: number, hp: number, weapon: Weapon): Character {
  return {
    x: x,
    y: y,
    dir: dir,
    hp: hp,
    weapon: weapon,
    speed: 1,
  };
}

export function newReadyWeapon(): Weapon {
  return {
    xOffset: 0,
    yOffset: 0,
    fireRate: 10,
    ready: true,
    fireRateCountdown: 0,
    dir: -1,
  };
}

export function newReadyWeaponWithOffset(xOffset: number, yOffset: number): Weapon {
  return {
    xOffset: xOffset,
    yOffset: yOffset,
    fireRate: 10,
    ready: true,
    fireRateCountdown: 0,
    dir: -1,
  };
}

export function newNotReadyWeapon(): Weapon {
  return {
    xOffset: 0,
    yOffset: 0,
    fireRate: 10,
    ready: false,
    fireRateCountdown: 10,
    dir: -1,
  };
}

export function newSpriteBody(x: number, y: number): SpriteBody {
  return { x: x, y: x, enable: true, center: { x: x, y: y } };
}

export function newDisabledSpritedBody(x: number, y: number): SpriteBody {
  return { x: x, y: x, enable: false, center: { x: x, y: y } };
}

export function newGame(): GameState {
  return {
    player: newCharacter(32, 0, 1),
    enemy: newCharacter(32, 0, 1),
    playerBullets: [],
    enemyBullets: [],
    obstacles: [],
  };
}

export function newGameWithPlayer(player: Character): GameState {
  return {
    player: player,
    enemy: newCharacter(32, 0, 1),
    playerBullets: [],
    enemyBullets: [],
    obstacles: [],
  };
}

export function newGameWithPlayerAndBullets(player: Character, bullets: Array<Bullet>): GameState {
  return {
    player: player,
    enemy: newCharacter(32, 0, 1),
    playerBullets: bullets,
    enemyBullets: [],
    obstacles: [],
  };
}

export function newGameWithCharactersAndBullets(
  player: Character,
  enemy: Character,
  playerBullets: Array<Bullet>,
  enemyBullets: Array<Bullet>,
): GameState {
  return {
    player: player,
    enemy: enemy,
    playerBullets: playerBullets,
    enemyBullets: enemyBullets,
    obstacles: [],
  };
}

export function newGameWithEnemy(enemy: Character): GameState {
  return {
    player: newCharacter(32, 0, 1),
    enemy: enemy,
    playerBullets: [],
    enemyBullets: [],
    obstacles: [],
  };
}

export function newSprite(x: number, y: number): Sprite {
  return {
    x: x,
    y: y,
    visible: true,
    body: {
      enable: true,
    },
  };
}

export function newHiddenSprite(x: number, y: number): Sprite {
  return {
    x: x,
    y: y,
    visible: false,
    body: {
      enable: true,
    },
  };
}
