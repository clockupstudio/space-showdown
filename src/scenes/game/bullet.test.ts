import { addCharacterBullet, moveBulletsSprites, removeUnusedBullet, updateBullet, updateBullets } from './bullet';
import { Controls } from './game';
import {
  newCharacter,
  newCharacterWithWeapon,
  newReadyWeaponWithOffset,
  newNotReadyWeapon,
  newCharacterWithWeaponAt,
  newHiddenSprite,
  newSprite,
} from './gamestate.test.helper';

describe('updatePlayerBullets', () => {
  it('return empty list if given an empty list', () => {
    expect(updateBullets([])).toEqual([]);
  });

  it('Update Y position upward', () => {
    expect(updateBullets([{ x: 0, y: 60, dir: -1 }])).toEqual([{ x: 0, y: 59, dir: -1 }]);
  });

  it('Update only Y position', () => {
    expect(
      updateBullets([
        { x: 0, y: 60, dir: -1 },
        { x: 10, y: 60, dir: -1 },
      ]),
    ).toEqual([
      { x: 0, y: 59, dir: -1 },
      { x: 10, y: 59, dir: -1 },
    ]);
  });
});

describe('updateBullet', () => {
  it('Update y position upward', () => {
    expect(updateBullet({ x: 0, y: 60, dir: -1 })).toEqual({ x: 0, y: 59, dir: -1 });
  });
});

describe('addPlayerBullet', () => {
  const PLAYER_Y = 56;

  it('return the same list as input if controls is not SHOOT', () => {
    expect(addCharacterBullet([], Controls.IDLE, newCharacter(0, 0, 1))).toEqual([]);
    expect(addCharacterBullet([], Controls.LEFT, newCharacter(0, 0, 1))).toEqual([]);
    expect(addCharacterBullet([], Controls.RIGHT, newCharacter(0, 0, 1))).toEqual([]);
  });

  it('add bullet to the list when player hit SHOOT key and Weapon is ready', () => {
    expect(
      addCharacterBullet([], Controls.SHOOT, newCharacterWithWeaponAt(0, 56, 0, 1, newReadyWeaponWithOffset(0, -10))),
    ).toEqual([{ x: 0, y: PLAYER_Y - 10, dir: -1 }]);
  });

  it('do not add bullet to the list when player hit SHOOT key but Weapon is not ready', () => {
    expect(addCharacterBullet([], Controls.SHOOT, newCharacterWithWeapon(0, 0, 1, newNotReadyWeapon()))).toEqual([]);
  });
});

describe('moveBulletSprites', () => {
  it('Always hide the sprite', () => {
    const playerBulletSprites = [newSprite(0, 0)];

    moveBulletsSprites([], playerBulletSprites);

    expect(playerBulletSprites).toEqual([newHiddenSprite(0, 0)]);
  });

  it('Update sprite position and set visible to true', () => {
    const playerBulletSprites = [newSprite(0, 0)];

    moveBulletsSprites([{ dir: -1, x: 10, y: 10 }], playerBulletSprites);

    expect(playerBulletSprites).toEqual([newSprite(10, 10)]);
  });

  it('Update and display only bullets available in the given state', () => {
    const playerBulletSprites = [newHiddenSprite(0, 0), newHiddenSprite(0, 0)];

    moveBulletsSprites([{ dir: -1, x: 10, y: 10 }], playerBulletSprites);

    expect(playerBulletSprites).toEqual([newSprite(10, 10), newHiddenSprite(0, 0)]);
  });
});

describe('removeUnusedBullets', () => {
  it('return empty list if input is empty', () => {
    expect(removeUnusedBullet([])).toEqual([]);
  });

  it('remove bullet from list when bullet y is lower than 0', () => {
    expect(removeUnusedBullet([{ x: 0, y: 0, dir: -1 }])).toEqual([{ x: 0, y: 0, dir: -1 }]);
    expect(
      removeUnusedBullet([
        { x: 0, y: 0, dir: -1 },
        { x: 0, y: -1, dir: -1 },
      ]),
    ).toEqual([{ x: 0, y: 0, dir: -1 }]);
  });

  it('remove bullet from list when bullet y is greater than 64', () => {
    expect(removeUnusedBullet([{ x: 0, y: 64, dir: 1 }])).toEqual([{ x: 0, y: 64, dir: 1 }]);
    expect(
      removeUnusedBullet([
        { x: 0, y: 64, dir: 1 },
        { x: 0, y: 65, dir: 1 },
      ]),
    ).toEqual([{ x: 0, y: 64, dir: 1 }]);
  });
});
