import { Scene, SceneData } from './scene';

export class MockScene implements Scene {
  nextScene: string;
  nextSceneData: SceneData;
  startHasBeenCalled = false;

  start(sceneName: string, sceneData: SceneData): void {
    this.startHasBeenCalled = true;
    this.nextSceneData = sceneData;
    this.nextScene = sceneName;
  }
}
