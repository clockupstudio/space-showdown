export const levels = [
  {
    enemyHp: 3,
    enemyColor: 1,
    enemySpeed: 0.1,
  },
  {
    enemyHp: 5,
    enemyColor: 2,
    enemySpeed: 0.2,
  },
  {
    enemyHp: 10,
    enemyColor: 3,
    enemySpeed: 0.5,
  },
];
