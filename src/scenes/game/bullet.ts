import { Character } from './character';
import { Controls, Sprite } from './game';

// Bullet is Bullet(y, dir)
// Interp. the current state of a bullet position
export class Bullet {
  x: number;
  y: number;
  dir: number;
}

// ListOfBullet -> ListOfBullet
// Produce list of bullet with update position by their direction.
// stub
// export function updatePlayerBullets(bullets: Array<Bullet>): Array<Bullet> {
//   return bullets;
// }
export function updateBullets(bullets: Array<Bullet>): Array<Bullet> {
  if (bullets.length == 0) {
    return [];
  }
  return [updateBullet(bullets[0])].concat(updateBullets(bullets.slice(1)));
}

// Bullet -> Bullet
// Produce Bullet with updated postion by its direction
// stub
// export function updateBullet(bullet: Bullet): Bullet {
//   return bullet;
// }
export function updateBullet(bullet: Bullet): Bullet {
  return {
    dir: bullet.dir,
    x: bullet.x,
    y: bullet.y + bullet.dir,
  };
}

// ListOfBullet Controls Character -> ListOfBullet
// produce add new bullet when player hit SHOOT.
export function addCharacterBullet(
  listOfBullet: Array<Bullet>,
  controlKey: Controls,
  character: Character,
): Array<Bullet> {
  if (controlKey != Controls.SHOOT) {
    return listOfBullet;
  }
  if (!character.weapon.ready) {
    return listOfBullet;
  }
  return listOfBullet.concat([
    {
      x: character.x + character.weapon.xOffset,
      y: character.y + character.weapon.yOffset,
      dir: character.weapon.dir,
    },
  ]);
}

// ListOfBullet ListOfSprite -> ()
// Update position and visibility of each bullet sprite in the given array
// stub
// export function movePlayerBulletsSprites(bullets: Array<Bullet>, bulletSprites: Array<Sprite>): void {
//   //
// }
export function moveBulletsSprites(bullets: Array<Bullet>, bulletSprites: Array<Sprite>): void {
  bulletSprites.forEach((pbs) => {
    pbs.visible = false;
  });
  bullets.forEach((b, i) => {
    bulletSprites[i].x = b.x;
    bulletSprites[i].y = b.y;
    bulletSprites[i].visible = true;
    bulletSprites[i].body.enable = true;
  });
}

// ListOfBullet -> ListOfBullet
// Produce list of bullet filtered by out of camera bullet
export function removeUnusedBullet(bullets: Array<Bullet>): Array<Bullet> {
  if (bullets.length == 0) {
    return [];
  } else if (bullets[0].y < 0) {
    return removeUnusedBullet(bullets.slice(1));
  } else if (bullets[0].y > 64) {
    return removeUnusedBullet(bullets.slice(1));
  } else {
    return [bullets[0]].concat(removeUnusedBullet(bullets.slice(1)));
  }
}
