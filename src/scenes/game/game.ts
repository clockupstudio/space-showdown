import * as Phaser from 'phaser';
import { moveBulletsSprites } from './bullet';
import { moveCharacterSprites } from './character';
import { EnemyBehavior, enemyControl, updateEnemyBehavior } from './enemybehavior';
import {
  GameState,
  readInput,
  readPlayerBulletCollisions,
  readEnemiesHit,
  updateGame,
  disableCollidedBullet,
  removeCollidedCache,
  moveCharacterSprite,
  readEnemyBehavior,
  readEnemyBulletCollisions,
  readPlayerHit,
} from './gamestate';
import { levels } from './levels';
import { SceneData, goToNextScene, goToGameOverScene } from './scene';

export default class PhaserCharacter extends Phaser.GameObjects.Sprite {
  body: Phaser.Physics.Arcade.Body;
}

const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
  active: false,
  visible: false,
  key: 'Game',
};

const PLAYER_Y = 56;
const ENEMY_Y = 10;
const OBSTACLE_SPEED = 0.4;

export enum Controls {
  LEFT,
  RIGHT,
  SHOOT,
  IDLE,
}

export interface Sprite {
  x: number;
  y: number;
  visible: boolean;
  body: {
    enable: boolean;
  };
}

export interface SpriteBody {
  x: number;
  y: number;
  enable: boolean;
  center: {
    x: number;
    y: number;
  };
}

export interface GameObjectWithBody {
  body: SpriteBody;
}

export class GameScene extends Phaser.Scene {
  private playerSprite: PhaserCharacter;
  private enemySprite: PhaserCharacter;

  private enemyBulletSprites: PhaserCharacter[] = [];
  private enemyBulletGroup: Phaser.Physics.Arcade.Group;

  private playerBulletSprites: PhaserCharacter[] = [];
  private playerBulletGroup: Phaser.Physics.Arcade.Group;

  private obstacleSprites: PhaserCharacter[] = [];
  private obstacleGroup: Phaser.Physics.Arcade.Group;

  private collidedPlayerBullets: Array<SpriteBody> = [];
  private collidedEnemies: Array<SpriteBody> = [];

  private collidedPlayers: Array<SpriteBody> = [];
  private collidedEnemyBullets: Array<SpriteBody> = [];

  private gameState: GameState;
  private enemyBehavior: EnemyBehavior;

  private level = 0;

  constructor() {
    super(sceneConfig);
  }

  public init(data: SceneData): void {
    if ('level' in data) {
      this.level = data.level;
    }
  }

  preload(): void {
    this.load.spritesheet('character', 'assets/sprites/character.png', { frameWidth: 10, frameHeight: 10 });
    this.load.spritesheet('bullet', 'assets/sprites/bullet.png', { frameWidth: 2, frameHeight: 2 });
    this.load.spritesheet('obstacle', 'assets/sprites/obstacle.png', { frameWidth: 8, frameHeight: 8 });
  }

  public create(): void {
    this.playerSprite = new PhaserCharacter(this, 0, 0, 'character', 0);
    this.add.existing(this.playerSprite);
    this.physics.add.existing(this.playerSprite);

    this.playerBulletGroup = this.physics.add.group();
    this.playerBulletSprites = this.prepareBulletSprites(this.playerBulletSprites, this.playerBulletGroup, 100);

    this.enemySprite = new PhaserCharacter(this, 0, 0, 'character', levels[this.level].enemyColor);
    this.add.existing(this.enemySprite);
    this.physics.add.existing(this.enemySprite);

    this.enemyBulletGroup = this.physics.add.group();
    this.enemyBulletSprites = this.prepareBulletSprites(this.enemyBulletSprites, this.enemyBulletGroup, 100);

    this.physics.add.collider(this.enemySprite, this.playerBulletGroup, (enemy, bullet) => {
      this.collidedEnemies.push(enemy.body);
      this.collidedPlayerBullets.push(bullet.body);
    });

    this.physics.add.collider(this.playerSprite, this.enemyBulletGroup, (player, bullet) => {
      this.collidedPlayers.push(player.body);
      this.collidedEnemyBullets.push(bullet.body);
    });

    this.obstacleGroup = this.physics.add.group();
    this.obstacleSprites = this.prepareObstacles(this.obstacleSprites, this.obstacleGroup, 20);

    this.physics.add.collider(this.obstacleGroup, this.enemyBulletGroup, (obstacle, bullet) => {
      this.collidedEnemyBullets.push(bullet.body);
    });

    this.physics.add.collider(this.obstacleGroup, this.playerBulletGroup, (obstacle, bullet) => {
      this.collidedPlayerBullets.push(bullet.body);
    });

    this.gameState = {
      player: {
        x: 32,
        y: PLAYER_Y,
        dir: 0,
        hp: 3,
        weapon: {
          dir: -1,
          xOffset: 0,
          yOffset: -(this.playerSprite.height / 2 + 1),
          fireRate: 11,
          fireRateCountdown: 0,
          ready: true,
        },
        speed: 1,
      },
      enemy: {
        x: 32,
        y: ENEMY_Y,
        dir: 0,
        hp: levels[this.level].enemyHp,
        weapon: {
          dir: 1,
          xOffset: 0,
          yOffset: this.enemySprite.height / 2 + 1,
          fireRate: 11,
          fireRateCountdown: 0,
          ready: true,
        },
        speed: levels[this.level].enemySpeed,
      },
      playerBullets: [],
      enemyBullets: [],
      obstacles: [
        { x: 4, y: 28, dir: -1, hp: 100, speed: OBSTACLE_SPEED },
        { x: 20, y: 38, dir: -1, hp: 100, speed: OBSTACLE_SPEED },
        { x: 36, y: 28, dir: -1, hp: 100, speed: OBSTACLE_SPEED },
        { x: 52, y: 38, dir: -1, hp: 100, speed: OBSTACLE_SPEED },
        { x: 66, y: 28, dir: -1, hp: 100, speed: OBSTACLE_SPEED },
      ],
    };

    this.enemyBehavior = {
      shootingCountdown: 0,
    };
  }

  prepareObstacles(
    obstacleSprites: PhaserCharacter[],
    obstacleGroup: Phaser.Physics.Arcade.Group,
    numbers: number,
  ): PhaserCharacter[] {
    if (numbers == 0) {
      return [];
    }

    const sprite = new PhaserCharacter(this, -10, -10, 'obstacle');
    sprite.visible = false;

    this.add.existing(sprite);
    obstacleGroup.add(sprite);
    sprite.body.enable = false;

    return this.prepareObstacles(obstacleSprites, obstacleGroup, numbers - 1).concat([sprite]);
  }

  private prepareBulletSprites(
    listOfBullet: PhaserCharacter[],
    group: Phaser.Physics.Arcade.Group,
    numbers,
  ): PhaserCharacter[] {
    if (numbers == 0) {
      return [];
    }

    const sprite = new PhaserCharacter(this, -100, -100, 'bullet');
    sprite.visible = false;

    this.add.existing(sprite);
    group.add(sprite);
    sprite.body.enable = false;

    return this.prepareBulletSprites(listOfBullet, group, numbers - 1).concat([sprite]);
  }

  public update(): void {
    this.enemyBehavior = updateEnemyBehavior(this.enemyBehavior);

    this.gameState = readInput(this.gameState, this.mapInput(this.input.keyboard.createCursorKeys()));
    this.gameState = readEnemyBehavior(this.gameState, enemyControl(this.enemyBehavior, this.gameState));
    this.gameState = readPlayerBulletCollisions(this.gameState, this.collidedPlayerBullets);
    this.gameState = readEnemyBulletCollisions(this.gameState, this.collidedEnemyBullets);
    this.gameState = readEnemiesHit(this.gameState, this.collidedEnemies);
    this.gameState = readPlayerHit(this.gameState, this.collidedPlayers);

    this.gameState = updateGame(this.gameState);

    disableCollidedBullet(this.collidedPlayerBullets);
    disableCollidedBullet(this.collidedEnemyBullets);

    removeCollidedCache(this.collidedEnemies);
    removeCollidedCache(this.collidedPlayerBullets);
    removeCollidedCache(this.collidedEnemyBullets);

    moveCharacterSprite(this.gameState.player, this.playerSprite);
    moveBulletsSprites(this.gameState.playerBullets, this.playerBulletSprites);

    moveCharacterSprite(this.gameState.enemy, this.enemySprite);
    moveBulletsSprites(this.gameState.enemyBullets, this.enemyBulletSprites);

    moveCharacterSprites(this.gameState.obstacles, this.obstacleSprites);

    goToNextScene(this.gameState, this.scene, this.level);
    goToGameOverScene(this.gameState, this.scene);
  }

  private mapInput(cursorKeys: Phaser.Types.Input.Keyboard.CursorKeys): Controls {
    if (cursorKeys.left.isDown) {
      return Controls.LEFT;
    } else if (cursorKeys.right.isDown) {
      return Controls.RIGHT;
    } else if (cursorKeys.space.isDown) {
      return Controls.SHOOT;
    }
    return Controls.IDLE;
  }
}
