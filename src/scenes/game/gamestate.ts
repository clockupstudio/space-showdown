import { addCharacterBullet, Bullet, removeUnusedBullet, updateBullets } from './bullet';
import {
  blockCharacter,
  Character,
  countdownWeapon,
  readCharacterShooting,
  updateCharacter,
  updateCharacters,
} from './character';
import { Sprite, Controls, SpriteBody } from './game';

// Data definition
// Game is Game(player, playerBullets, enemy, enemyBullets, obstacles)
// Interp. the current state of the game
// with the current player, enemy, playerBullets, enemyBullets
// and obstacles position
export class GameState {
  player: Character;
  enemy: Character;
  playerBullets: Array<Bullet>;
  enemyBullets: Array<Bullet>;
  obstacles: Array<Character>;
}

//Template for game
/*
  function fn-for-game(game: Game) {
    ...(fn-for-player(game.player)
        fn-for-enemy(game.enemy)
        fn-for-playerBullets(game.playerBullets)
        fn-for-enemyBullets(game.enemyBullets)
        fn-for-obstacles(game.obstacles))
  }
  */

// Game -> Game
// Produce a next state of the game
// Stub
// export function updateGame(game: Game): Game {
//   return game;
// }
export function updateGame(game: GameState): GameState {
  return {
    player: countdownWeapon(blockCharacter(updateCharacter(game.player))),
    enemy: countdownWeapon(updateCharacter(game.enemy)),
    playerBullets: removeUnusedBullet(updateBullets(game.playerBullets)),
    enemyBullets: removeUnusedBullet(updateBullets(game.enemyBullets)),
    obstacles: resetObstacles(updateCharacters(game.obstacles)),
  };
}

// Player Sprite -> ()
// Updated player's sprite position
export function moveCharacterSprite(player: Character, sprite: Sprite): void {
  sprite.x = player.x;
  sprite.y = player.y;
}

// Game Controls -> Game
// Produce a game with update state from input
// stub
// export function readInput(game: Game, controlKey: Controls): Game {
//   throw new Error('Function not implemented.');
// }
export function readInput(game: GameState, controlKey: Controls): GameState {
  return {
    player: readCharacterShooting(readCharacterDirection(game.player, controlKey), controlKey),
    enemy: game.enemy,
    playerBullets: addCharacterBullet(game.playerBullets, controlKey, game.player),
    enemyBullets: game.enemyBullets,
    obstacles: game.obstacles,
  };
}

// Game Controls -> Game
// Produce a game with update from enemy's behavior
export function readEnemyBehavior(game: GameState, controlKey: Controls): GameState {
  return {
    player: game.player,
    enemy: readCharacterShooting(readCharacterDirection(game.enemy, controlKey), controlKey),
    playerBullets: game.playerBullets,
    enemyBullets: addCharacterBullet(game.enemyBullets, controlKey, game.enemy),
    obstacles: game.obstacles,
  };
}

// Player Controls -> Player
// Produce player with update direction from the given key
// stub
// export function readPlayerDirection(player: Character, controlKey: Controls): Character {
//   return player;
// }
export function readCharacterDirection(player: Character, controlKey: Controls): Character {
  switch (controlKey) {
    case Controls.LEFT: {
      return { x: player.x, y: player.y, dir: -1, hp: player.hp, weapon: player.weapon, speed: player.speed };
    }
    case Controls.RIGHT: {
      return { x: player.x, y: player.y, dir: 1, hp: player.hp, weapon: player.weapon, speed: player.speed };
    }
    case Controls.IDLE: {
      return { x: player.x, y: player.y, dir: 0, hp: player.hp, weapon: player.weapon, speed: player.speed };
    }
    default: {
      return { x: player.x, y: player.y, dir: player.dir, hp: player.hp, weapon: player.weapon, speed: player.speed };
    }
  }
}

// Game ListOfSprite -> Game
// Produce game with updated collisions bullet
export function readPlayerBulletCollisions(game: GameState, sprites: Array<SpriteBody>): GameState {
  return {
    enemy: game.enemy,
    enemyBullets: game.enemyBullets,
    obstacles: game.obstacles,
    player: game.player,
    playerBullets: removeCollidedBullets(game.playerBullets, sprites),
  };
}

// Game ListOfSprite -> Game
// Produce game with updated enemy collisions bullet
export function readEnemyBulletCollisions(game: GameState, sprites: Array<SpriteBody>): GameState {
  return {
    enemy: game.enemy,
    enemyBullets: removeCollidedBullets(game.enemyBullets, sprites),
    obstacles: game.obstacles,
    player: game.player,
    playerBullets: game.playerBullets,
  };
}

// ListOfBullet ListOfSprite -> ListOfBullet
// Produce a list of bullet filtered by matched with the given sprite position
export function removeCollidedBullets(bullets: Array<Bullet>, bodies: Array<SpriteBody>): Array<Bullet> {
  if (bodies.length == 0) {
    return bullets;
  }

  return bullets.filter((bullet) => {
    return bodies.some((body) => {
      return !(bullet.x == body.center.x && bullet.y == body.center.y);
    });
  });
}

// ListOfSprite -> ()
// Disable sprite physics body of every sprite in the list
export function disableCollidedBullet(sprites: Array<SpriteBody>): void {
  sprites.forEach((sb) => {
    sb.enable = false;
  });
}

// ListOfBody -> ()
// remove all cached bullet body
export function removeCollidedCache(collidedBullets: SpriteBody[]): void {
  collidedBullets.length = 0;
}

// ListOfBody ListOfBullet -> ()
// enable physics body of the bullets in the current state
export function enableExistingBullets(bodies: Array<SpriteBody>, bullets: Array<Bullet>): void {
  bodies
    .filter((body) =>
      bullets.some((bullet) => {
        return bullet.x == body.center.x && bullet.y == body.center.y;
      }),
    )
    .forEach((body: SpriteBody) => (body.enable = true));
}

// Game ListOfBody -> Game
export function readEnemiesHit(gameState: GameState, collidedEnemies: SpriteBody[]): GameState {
  return {
    enemy: reduceHealth(gameState.enemy, collidedEnemies),
    enemyBullets: gameState.enemyBullets,
    obstacles: gameState.obstacles,
    player: gameState.player,
    playerBullets: gameState.playerBullets,
  };
}

// Game ListOfBody -> Game
// Produce game with updated player's hp from the number of collisions
export function readPlayerHit(gameState: GameState, collidedPlayers: SpriteBody[]): GameState {
  return {
    enemy: gameState.enemy,
    enemyBullets: gameState.enemyBullets,
    obstacles: gameState.obstacles,
    player: reduceHealth(gameState.player, collidedPlayers),
    playerBullets: gameState.playerBullets,
  };
}

// Character ListOfBody -> Character
// Produce Character with reduced helth.
export function reduceHealth(enemy: Character, collidedEnemies: SpriteBody[]): Character {
  if (collidedEnemies.length == 0) {
    return enemy;
  }

  return reduceHealth(
    {
      x: enemy.x,
      y: enemy.y,
      dir: enemy.dir,
      weapon: enemy.weapon,
      hp: enemy.hp - 1,
      speed: enemy.speed,
    },
    collidedEnemies.slice(1),
  );
}

// ListOfCharacter -> ListOfCharacter
// Produce a list with reset the position of character which completely outside od the screen
// !!!
export function resetObstacles(obstacles: Array<Character>): Array<Character> {
  if (obstacles.length == 0) {
    return [];
  }
  if (obstacles[0].x <= -4) {
    return resetObstacles(obstacles.slice(1)).concat([resetObstacle(obstacles[0])]);
  }
  return [obstacles[0]].concat(resetObstacles(obstacles.slice(1)));
}

// Character -> Character
// Produce a character with reset position to the right most of the screen
// !!!
export function resetObstacle(obstacle: Character): Character {
  return {
    x: 64 + 4,
    dir: obstacle.dir,
    hp: obstacle.hp,
    speed: obstacle.speed,
    y: obstacle.y,
    weapon: obstacle.weapon,
  };
}
