// Player is Player(x, dir, hp, Weapon)
// Interp. the current state of the player ship

import { Controls, Sprite } from './game';

// with the current player vertical position, direction, hp and Weapon
export class Character {
  x: number;
  y: number;
  dir: number;
  hp: number;
  weapon?: Weapon;
  speed: number;
}

// Weapon is new Weapon(fireRate, fireRatecountdown, ready)
// Interp the current state of weapon
// with the current fire rate, fireRateCountdown and ready.
export class Weapon {
  fireRate: number;
  fireRateCountdown: number;
  ready: boolean;
  xOffset: number;
  yOffset: number;
  dir: number;
}

// Character, Controls -> Player
// Produce character with update weapon status
export function readCharacterShooting(character: Character, control: Controls): Character {
  return {
    x: character.x,
    y: character.y,
    dir: character.dir,
    hp: character.hp,
    weapon: weaponNotReady(character.weapon, control),
    speed: character.speed,
  };
}

// Weapon, controls -> Player
// Produce weapon with update ready status from a given control
// ready flag is false when player pressed the SHOOT key.
export function weaponNotReady(weapon: Weapon, control: Controls): Weapon {
  if (control != Controls.SHOOT) {
    return weapon;
  }
  return {
    fireRateCountdown: weapon.fireRate,
    fireRate: weapon.fireRate,
    ready: false,
    xOffset: weapon.xOffset,
    yOffset: weapon.yOffset,
    dir: weapon.dir,
  };
}

// ListofCharacter -> ListOfCharacter
// Produce a list of updated characters
export function updateCharacters(characters: Array<Character>): Array<Character> {
  return characters.map(updateCharacter);
}

// Character -> Character
// Produce a Character with updated position
export function updateCharacter(player: Character): Character {
  const next = player.x + player.dir * player.speed;
  return {
    x: next,
    y: player.y,
    dir: player.dir,
    hp: player.hp,
    weapon: player.weapon,
    speed: player.speed,
  };
}

// Character -> Character
// Produce a character with x position is...
// 0 if x position is lower or equals to 0
// 64 if x position is greater than or equals to 64
// !!!
export function blockCharacter(character: Character): Character {
  return character;
}

// Character -> Character
// Produce character with updated weapon state
// if ready is false then decrease the fireRateCountdown
// if fireRateCountdown is 0 then set weapon ready to true
export function countdownWeapon(character: Character): Character {
  if (character.weapon.fireRateCountdown == 0) {
    return {
      x: character.x,
      y: character.y,
      dir: character.dir,
      hp: character.hp,
      weapon: {
        dir: character.weapon.dir,
        fireRate: character.weapon.fireRate,
        fireRateCountdown: 0,
        ready: true,
        xOffset: character.weapon.xOffset,
        yOffset: character.weapon.yOffset,
      },
      speed: character.speed,
    };
  }
  return {
    x: character.x,
    y: character.y,
    dir: character.dir,
    hp: character.hp,
    weapon: {
      dir: character.weapon.dir,
      fireRate: character.weapon.fireRate,
      fireRateCountdown: character.weapon.fireRateCountdown - 1,
      ready: false,
      xOffset: character.weapon.xOffset,
      yOffset: character.weapon.yOffset,
    },
    speed: character.speed,
  };
}

// ListOfCharacter Sprite -> ()
// Update all the sprites position in the given list
export function moveCharacterSprites(characters: Array<Character>, sprites: Array<Sprite>): void {
  sprites.forEach((s) => {
    s.visible = false;
  });

  characters.forEach((c, i) => {
    sprites[i].x = c.x;
    sprites[i].y = c.y;
    sprites[i].visible = true;
    sprites[i].body.enable = true;
  });
}
