import { GameState } from './gamestate';
import { levels } from './levels';

export interface Scene {
  start(sceneName: string, data?: SceneData);
}

export interface SceneData {
  level: number;
}
// Game Scene Level -> ()
// Execute change scene when enemy is dead
export function goToNextScene(gameState: GameState, scene: Scene, level: number): void {
  if (gameState.enemy.hp > 0) {
    return;
  }

  const nextLevel = level + 1;

  if (nextLevel > levels.length - 1) {
    scene.start('Ending');
  } else {
    scene.start('Game', { level: nextLevel });
  }
}

// Game Scene -> ()
// go to gameover scene when player is dead
export function goToGameOverScene(gameState: GameState, scene: Scene): void {
  if (gameState.player.hp > 0) {
    return;
  }

  scene.start('Gameover');
}
