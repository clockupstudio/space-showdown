// Data definitions;

import { Controls } from './game';
import { GameState } from './gamestate';

// EnemyBehavior is EnemyBehavior(shootingCountdown)
// Interpret current behavior of the enemy
// With the countdown for execute shooting
export class EnemyBehavior {
  shootingCountdown: number;
}

// Functions

// EnemyBehavior GameState -> Controls
// Produce Controls.SHOOT when shootingCountdown is 0
// Produce Controls.IDLE when shootingCountdown is greater than 0
// Produce Controls.LEFT when player is moving to the left
// Produce Controls.RIGHT when player is moving to the right
export function enemyControl(eb: EnemyBehavior, game: GameState): Controls {
  if (eb.shootingCountdown == 0) {
    return Controls.SHOOT;
  }
  if (game.player.x < game.enemy.x) {
    return Controls.LEFT;
  }
  if (game.player.x > game.enemy.x) {
    return Controls.RIGHT;
  }
  return Controls.IDLE;
}

// EnemyBehavior -> Controls
// Reduce shooting countdown
// Reset shooting countdown when reached 0
export function updateEnemyBehavior(eb: EnemyBehavior): EnemyBehavior {
  if (eb.shootingCountdown == 0) {
    return { shootingCountdown: 50 };
  }
  return { shootingCountdown: eb.shootingCountdown - 1 };
}
