import {
  GameState,
  updateGame,
  readInput,
  readCharacterDirection,
  readPlayerBulletCollisions,
  removeCollidedBullets,
  disableCollidedBullet,
  removeCollidedCache,
  enableExistingBullets,
  readEnemiesHit,
  reduceHealth,
  readEnemyBehavior,
  readEnemyBulletCollisions,
  readPlayerHit,
  resetObstacles,
  resetObstacle,
} from './gamestate';
import {
  newCharacter,
  newCharacterWithWeapon,
  newCharacterWithWeaponAt,
  newDisabledSpritedBody,
  newGame,
  newGameWithCharactersAndBullets,
  newGameWithEnemy,
  newGameWithPlayer,
  newGameWithPlayerAndBullets,
  newNotReadyWeapon,
  newReadyWeapon,
  newSpriteBody,
} from './gamestate.test.helper';
import { Controls, SpriteBody } from './game';

describe('Update game', () => {
  it('Game with no moves', () => {
    const game: GameState = newGame();

    expect(updateGame(game)).toEqual(newGame());
  });

  it('Game with player wants to move to the left', () => {
    const game: GameState = newGameWithPlayer(newCharacter(32, -1, 1));

    expect(updateGame(game)).toEqual(newGameWithPlayer(newCharacter(31, -1, 1)));
  });
});

describe('readInput', () => {
  it('Set direction to 0 when the player does not hit left or right', () => {
    expect(
      readInput(newGameWithCharactersAndBullets(newCharacter(32, 0, 1), newCharacter(32, 0, 1), [], []), Controls.IDLE),
    ).toEqual(
      newGameWithCharactersAndBullets(
        newCharacter(32, 0, 1),
        newCharacterWithWeapon(32, 0, 1, newReadyWeapon()),
        [],
        [],
      ),
    );
  });

  it('Add bullet to the scene when the player press SHOOT', () => {
    expect(
      readInput(
        newGameWithCharactersAndBullets(
          newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
          newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
          [],
          [],
        ),
        Controls.SHOOT,
      ),
    ).toEqual(
      newGameWithCharactersAndBullets(
        newCharacterWithWeaponAt(32, 56, 0, 1, newNotReadyWeapon()),
        newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
        [{ x: 32, y: 56, dir: -1 }],
        [],
      ),
    );
  });
});

describe('readEnemyBehavior', () => {
  it('Do nothing when given IDLE', () => {
    expect(
      readEnemyBehavior(
        newGameWithCharactersAndBullets(
          newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
          newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
          [],
          [],
        ),
        Controls.IDLE,
      ),
    ).toEqual(
      newGameWithCharactersAndBullets(
        newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
        newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
        [],
        [],
      ),
    );
  });

  it('Add enemy bullet when given SHOOT', () => {
    expect(
      readEnemyBehavior(
        newGameWithCharactersAndBullets(
          newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
          newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
          [],
          [],
        ),
        Controls.SHOOT,
      ),
    ).toEqual(
      newGameWithCharactersAndBullets(
        newCharacterWithWeaponAt(32, 56, 0, 1, newReadyWeapon()),
        newCharacterWithWeaponAt(32, 56, 0, 1, newNotReadyWeapon()),
        [],
        [{ x: 32, y: 56, dir: -1 }],
      ),
    );
  });
});

describe('readPlayerDirection', () => {
  it('Set negative direction when player hit left', () => {
    expect(readCharacterDirection(newCharacter(0, 0, 1), Controls.LEFT)).toEqual(newCharacter(0, -1, 1));
  });

  it('Set positive direction when player hit right', () => {
    expect(readCharacterDirection(newCharacter(0, 0, 1), Controls.RIGHT)).toEqual(newCharacter(0, 1, 1));
  });

  it('Set 0 direction when player does not hit any input', () => {
    expect(readCharacterDirection(newCharacter(0, 0, 1), Controls.IDLE)).toEqual(newCharacter(0, 0, 1));
    expect(readCharacterDirection(newCharacter(0, -1, 1), Controls.IDLE)).toEqual(newCharacter(0, 0, 1));
    expect(readCharacterDirection(newCharacter(0, 1, 1), Controls.IDLE)).toEqual(newCharacter(0, 0, 1));
  });

  it('Ignore direction when player hit shoot', () => {
    expect(readCharacterDirection(newCharacter(0, 0, 1), Controls.SHOOT)).toEqual(newCharacter(0, 0, 1));
    expect(readCharacterDirection(newCharacter(0, 1, 1), Controls.SHOOT)).toEqual(newCharacter(0, 1, 1));
    expect(readCharacterDirection(newCharacter(0, -1, 1), Controls.SHOOT)).toEqual(newCharacter(0, -1, 1));
  });
});

describe('readPlayerBulletCollision', () => {
  it('return an empty list when given the empty list', () => {
    expect(
      readPlayerBulletCollisions(newGameWithPlayer(newCharacterWithWeapon(32, 0, 1, newReadyWeapon())), []),
    ).toEqual(newGameWithPlayer(newCharacterWithWeapon(32, 0, 1, newReadyWeapon())));
  });

  it('return the same list when given and empty collision', () => {
    expect(
      readPlayerBulletCollisions(
        newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [{ dir: -1, x: 0, y: 0 }]),
        [],
      ),
    ).toEqual(
      newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [{ dir: -1, x: 0, y: 0 }]),
    );
  });

  it('return game with removed bullet exists in the given array', () => {
    expect(
      readPlayerBulletCollisions(
        newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [{ dir: -1, x: 0, y: 0 }]),
        [newSpriteBody(0, 0)],
      ),
    ).toEqual(newGameWithPlayer(newCharacter(32, 0, 1)));
  });

  it('return game with removed only exaxtly matched bullet exists in the given array', () => {
    expect(
      readPlayerBulletCollisions(
        newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [
          { dir: -1, x: 0, y: 0 },
          { dir: -1, x: 10, y: 10 },
        ]),
        [newSpriteBody(0, 0)],
      ),
    ).toEqual(
      newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [{ dir: -1, x: 10, y: 10 }]),
    );
  });
});

describe('readEnemyBulletCollisions', () => {
  it('return an empty list when given the empty list', () => {
    expect(
      readEnemyBulletCollisions(newGameWithPlayer(newCharacterWithWeapon(32, 0, 1, newReadyWeapon())), []),
    ).toEqual(newGameWithPlayer(newCharacterWithWeapon(32, 0, 1, newReadyWeapon())));
  });

  it('return the same list when given and empty collision', () => {
    expect(
      readEnemyBulletCollisions(
        newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [{ dir: -1, x: 0, y: 0 }]),
        [],
      ),
    ).toEqual(
      newGameWithPlayerAndBullets(newCharacterWithWeapon(32, 0, 1, newReadyWeapon()), [{ dir: -1, x: 0, y: 0 }]),
    );
  });

  it('return game with removed bullet exists in the given array', () => {
    expect(
      readEnemyBulletCollisions(
        newGameWithCharactersAndBullets(
          newCharacterWithWeapon(32, 0, 1, newReadyWeapon()),
          newCharacterWithWeapon(32, 0, 1, newReadyWeapon()),
          [],
          [{ dir: -1, x: 0, y: 0 }],
        ),
        [newSpriteBody(0, 0)],
      ),
    ).toEqual(
      newGameWithCharactersAndBullets(
        newCharacterWithWeapon(32, 0, 1, newReadyWeapon()),
        newCharacterWithWeapon(32, 0, 1, newReadyWeapon()),
        [],
        [],
      ),
    );
  });
});

describe('removeCollidedBullets', () => {
  it('return empty list when given an empty sprite list', () => {
    expect(removeCollidedBullets([], [])).toEqual([]);
  });

  it('return the same list when given an empty sprite list', () => {
    expect(removeCollidedBullets([{ dir: -1, x: 10, y: 10 }], [])).toEqual([{ dir: -1, x: 10, y: 10 }]);
  });

  it('remove bullet with matched with one in the sprite list', () => {
    expect(removeCollidedBullets([{ dir: -1, x: 10, y: 10 }], [newSpriteBody(10, 10)])).toEqual([]);
  });

  it('remove bullet with matched with one in the sprite list only', () => {
    expect(
      removeCollidedBullets(
        [
          { dir: -1, x: 10, y: 10 },
          { dir: -1, x: 100, y: 100 },
        ],
        [newSpriteBody(10, 10)],
      ),
    ).toEqual([{ dir: -1, x: 100, y: 100 }]);
  });
});

describe('disableCollidedBullets', () => {
  it('disable the physics body on every sprite', () => {
    const sprites: Array<SpriteBody> = [];

    disableCollidedBullet(sprites);

    expect(sprites).toEqual([]);
  });

  it('disable the physics body on every sprite', () => {
    const sprites: Array<SpriteBody> = [newSpriteBody(0, 0)];

    disableCollidedBullet(sprites);

    expect(sprites).toEqual([newDisabledSpritedBody(0, 0)]);
  });
});

describe('removeCollidedBulletsCache', () => {
  it('Empty list', () => {
    const bullets = [];

    removeCollidedCache(bullets);

    expect(bullets).toEqual([]);
  });

  it('One element list', () => {
    const bullets: Array<SpriteBody> = [newSpriteBody(0, 0)];

    removeCollidedCache(bullets);

    expect(bullets).toEqual([]);
  });
});

// ListOfBody -> ()
// enable physics body of the bullets in the current state
// export function enableExistingBullets(bullets: Array<Bullet>, bodies: Array<SpriteBody>): void {
//   //
// }
describe('enableExistingBullets', () => {
  it('empty list', () => {
    const bodies = [];

    enableExistingBullets([], bodies);

    expect(bodies).toEqual([]);
  });

  it('set enable of the body to true', () => {
    const bodies = [newSpriteBody(0, 0)];

    enableExistingBullets(bodies, [{ x: 0, y: 0, dir: -1 }]);

    expect(bodies).toEqual([newSpriteBody(0, 0)]);
  });
});

describe('readEnemiesCollision', () => {
  it('reduce 0 hp when given an empty list', () => {
    expect(readEnemiesHit(newGameWithEnemy(newCharacter(0, 0, 1)), [])).toEqual(
      newGameWithEnemy(newCharacter(0, 0, 1)),
    );
  });

  it('reduce 1 hp when given an list with 1 element', () => {
    expect(readEnemiesHit(newGameWithEnemy(newCharacter(0, 0, 1)), [newSpriteBody(0, 0)])).toEqual(
      newGameWithEnemy(newCharacter(0, 0, 0)),
    );
  });

  it('reduce 2 hp when given an list with 2 elements', () => {
    expect(
      readEnemiesHit(newGameWithEnemy(newCharacter(0, 0, 10)), [newSpriteBody(0, 0), newSpriteBody(0, 0)]),
    ).toEqual(newGameWithEnemy(newCharacter(0, 0, 8)));
  });
});

describe('readPlayerCollisions', () => {
  it('reduce 0 hp when given an empty list', () => {
    expect(readPlayerHit(newGameWithPlayer(newCharacter(0, 0, 1)), [])).toEqual(
      newGameWithPlayer(newCharacter(0, 0, 1)),
    );
  });

  it('reduce 1 hp when given an list with 1 element', () => {
    expect(readPlayerHit(newGameWithPlayer(newCharacter(0, 0, 1)), [newSpriteBody(0, 0)])).toEqual(
      newGameWithPlayer(newCharacter(0, 0, 0)),
    );
  });

  it('reduce 2 hp when given an list with 2 elements', () => {
    expect(
      readPlayerHit(newGameWithPlayer(newCharacter(0, 0, 10)), [newSpriteBody(0, 0), newSpriteBody(0, 0)]),
    ).toEqual(newGameWithPlayer(newCharacter(0, 0, 8)));
  });
});

describe('reduceHealth', () => {
  it('reduce 0 hp when given an empty list', () => {
    expect(reduceHealth(newCharacter(0, 0, 1), [])).toEqual(newCharacter(0, 0, 1));
  });

  it('reduce 1 hp when given an list with 1 element', () => {
    expect(reduceHealth(newCharacter(0, 0, 1), [newSpriteBody(0, 0)])).toEqual(newCharacter(0, 0, 0));
  });

  it('reduce 2 hp when given an list with 2 elements', () => {
    expect(reduceHealth(newCharacter(0, 0, 10), [newSpriteBody(0, 0), newSpriteBody(0, 0)])).toEqual(
      newCharacter(0, 0, 8),
    );
  });
});

describe('resetObstacles', () => {
  it('return empty list when given an empty list', () => {
    expect(resetObstacles([])).toEqual([]);
  });

  it('return the same list if none of them out of the screen', () => {
    expect(resetObstacles([newCharacter(0, -1, 1)])).toEqual([newCharacter(0, -1, 1)]);
  });

  it('return list with reset obstacles if they go out of the screen', () => {
    expect(resetObstacles([newCharacter(-4, -1, 1)])).toEqual([newCharacter(64 + 4, -1, 1)]);
  });
});

describe('resetObstacle', () => {
  it('move character to the right origin of the screen', () => {
    expect(resetObstacle(newCharacter(-4, -1, 1))).toEqual(newCharacter(64 + 4, -1, 1));
  });
});
