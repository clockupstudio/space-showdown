import {
  updateCharacter,
  countdownWeapon,
  readCharacterShooting,
  weaponNotReady,
  moveCharacterSprites,
  updateCharacters,
  blockCharacter,
} from './character';
import { Controls, Sprite } from './game';
import { moveCharacterSprite } from './gamestate';
import {
  newCharacter,
  newCharacterAt,
  newCharacterWithWeapon,
  newHiddenSprite,
  newNotReadyWeapon,
  newReadyWeapon,
  newSprite,
} from './gamestate.test.helper';

describe('readCharacterShooting', () => {
  it('Set weapon ready status to false when player pressed SHOOT key', () => {
    expect(readCharacterShooting(newCharacterWithWeapon(0, 0, 1, newReadyWeapon()), Controls.SHOOT)).toEqual(
      newCharacterWithWeapon(0, 0, 1, newNotReadyWeapon()),
    );
  });

  it('Set fireRateCountdown to fireRate value when change from ready to not rady', () => {
    expect(readCharacterShooting(newCharacterWithWeapon(0, 0, 1, newReadyWeapon()), Controls.SHOOT)).toEqual(
      newCharacterWithWeapon(0, 0, 1, newNotReadyWeapon()),
    );
  });

  it('Ignore ready status when player pressed SHOOT key and weapon is not ready', () => {
    expect(readCharacterShooting(newCharacterWithWeapon(0, 0, 1, newNotReadyWeapon()), Controls.SHOOT)).toEqual(
      newCharacterWithWeapon(0, 0, 1, newNotReadyWeapon()),
    );
  });
});

describe('weaponNotReady', () => {
  it('set ready to false when player pressed SHOOT key', () => {
    expect(weaponNotReady(newReadyWeapon(), Controls.SHOOT)).toEqual({
      fireRate: 10,
      fireRateCountdown: 10,
      ready: false,
      xOffset: 0,
      yOffset: 0,
      dir: -1,
    });
  });
});

describe('updateCharacters', () => {
  it('return an empty list when given empty list', () => {
    expect(updateCharacters([])).toEqual([]);
  });

  it('Update character position in the list', () => {
    expect(updateCharacters([newCharacter(10, -1, 1)])).toEqual([newCharacter(9, -1, 1)]);
  });
});

describe('updateCharacter', () => {
  it('idle player ship when direction is 0', () => {
    expect(updateCharacter(newCharacter(0, 0, 1))).toEqual(newCharacter(0, 0, 1));
  });

  it('move player ship to the left when direction is negative', () => {
    expect(updateCharacter(newCharacter(32, -1, 1))).toEqual(newCharacter(32 - 1, -1, 1));
  });

  it('move player ship to the right when direction is positive', () => {
    expect(updateCharacter(newCharacter(32, 1, 1))).toEqual(newCharacter(32 + 1, 1, 1));
  });
});

describe('blockCharacter', () => {
  it('not allow player ship to go over the left of the screen', () => {
    expect(blockCharacter(newCharacter(0, -1, 1))).toEqual(newCharacter(0, -1, 1));
  });

  it('not allow player ship to go over the right of the screen', () => {
    expect(blockCharacter(newCharacter(64, 1, 1))).toEqual(newCharacter(64, 1, 1));
  });
});

describe('countdownWeapon', () => {
  it('reduce weapon firerateCountdown ', () => {
    expect(
      countdownWeapon(
        newCharacterWithWeapon(0, 0, 1, {
          dir: -1,
          xOffset: 0,
          yOffset: 0,
          fireRate: 10,
          fireRateCountdown: 10,
          ready: false,
        }),
      ),
    ).toEqual(
      newCharacterWithWeapon(0, 0, 1, {
        dir: -1,
        xOffset: 0,
        yOffset: 0,
        fireRate: 10,
        fireRateCountdown: 9,
        ready: false,
      }),
    );
  });

  it('stop countdown when firerateCountdown reach 0', () => {
    expect(
      countdownWeapon(
        newCharacterWithWeapon(0, 0, 1, {
          dir: -1,
          xOffset: 0,
          yOffset: 0,
          fireRate: 10,
          fireRateCountdown: 0,
          ready: false,
        }),
      ),
    ).toEqual(
      newCharacterWithWeapon(0, 0, 1, {
        dir: -1,
        xOffset: 0,
        yOffset: 0,
        fireRate: 10,
        fireRateCountdown: 0,
        ready: true,
      }),
    );
  });

  it('set ready to true when fireRatecountdow is 0', () => {
    expect(
      countdownWeapon(
        newCharacterWithWeapon(0, 0, 1, {
          dir: -1,
          xOffset: 0,
          yOffset: 0,
          fireRate: 10,
          fireRateCountdown: 0,
          ready: false,
        }),
      ),
    ).toEqual(
      newCharacterWithWeapon(0, 0, 1, {
        dir: -1,
        xOffset: 0,
        yOffset: 0,
        fireRate: 10,
        fireRateCountdown: 0,
        ready: true,
      }),
    );
  });
});

describe('movePlayerSprite', () => {
  it('set position og the given sprite by the given position', () => {
    const playerSprite: Sprite = newSprite(0, 0);

    moveCharacterSprite(newCharacterAt(32, 56, 1, 1), playerSprite);

    expect(playerSprite).toEqual(newSprite(32, 56));
  });
});

describe('moveCharacterSprites', () => {
  it('Always hide the sprite', () => {
    const charcterSprites = [newSprite(0, 0)];

    moveCharacterSprites([], charcterSprites);

    expect(charcterSprites).toEqual([newHiddenSprite(0, 0)]);
  });

  it('Update sprite position and set visible to true', () => {
    const characterSprites = [newSprite(0, 0)];

    moveCharacterSprites([newCharacterAt(32, 56, 1, 1)], characterSprites);

    expect(characterSprites).toEqual([newSprite(32, 56)]);
  });

  it('Update and display only bullets available in the given state', () => {
    const characterSprites = [newHiddenSprite(0, 0), newHiddenSprite(0, 0)];

    moveCharacterSprites([newCharacterAt(32, 56, 1, 1)], characterSprites);

    expect(characterSprites).toEqual([newSprite(32, 56), newHiddenSprite(0, 0)]);
  });
});
