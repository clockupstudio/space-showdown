import { enemyControl, updateEnemyBehavior } from './enemybehavior';
import { Controls } from './game';
import { newCharacter, newGameWithCharactersAndBullets, newGameWithPlayer } from './gamestate.test.helper';

describe('enemyControl', () => {
  it('Return SHOOT when sshootingCountdown is 0', () => {
    expect(enemyControl({ shootingCountdown: 0 }, newGameWithPlayer(newCharacter(0, -1, 1)))).toEqual(Controls.SHOOT);
  });

  it('Return IDLE when sshootingCountdown is greater than 0', () => {
    expect(
      enemyControl(
        { shootingCountdown: 1 },
        newGameWithCharactersAndBullets(newCharacter(30, -1, 1), newCharacter(30, -1, 1), [], []),
      ),
    ).toEqual(Controls.IDLE);
  });

  it('Return LEFT when plyer is on the left', () => {
    expect(
      enemyControl(
        { shootingCountdown: 1 },
        newGameWithCharactersAndBullets(newCharacter(30, -1, 1), newCharacter(32, -1, 1), [], []),
      ),
    ).toEqual(Controls.LEFT);
  });

  it('Return LEFT when plyer is on the left', () => {
    expect(
      enemyControl(
        { shootingCountdown: 1 },
        newGameWithCharactersAndBullets(newCharacter(32, -1, 1), newCharacter(30, -1, 1), [], []),
      ),
    ).toEqual(Controls.RIGHT);
  });
});

describe('updateEnemyBehavior', () => {
  it('reduce shooting countdown', () => {
    expect(updateEnemyBehavior({ shootingCountdown: 10 })).toEqual({ shootingCountdown: 9 });
  });

  it('reset shooting countdown when reached 0', () => {
    expect(updateEnemyBehavior({ shootingCountdown: 0 })).toEqual({ shootingCountdown: 50 });
  });
});
