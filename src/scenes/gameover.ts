const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
  active: false,
  visible: false,
  key: 'Gameover',
};

export class GameoverScene extends Phaser.Scene {
  constructor() {
    super(sceneConfig);
  }

  preload(): void {
    this.load.spritesheet('thankyou', 'assets/sprites/text.png', { frameWidth: 64, frameHeight: 64 });
  }

  create(): void {
    this.add.sprite(32, 32, 'thankyou', 1);
  }
}
